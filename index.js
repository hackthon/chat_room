var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var url = require('url');
var path = require('path');
var gid = 0;

app.use(express.static(path.join(__dirname + '/public')));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/:id', function(req, res) {
  res.sendFile(__dirname + '/index.html');
  gid = req.params.id;
  console.log("parse:" + gid);
//  console.log("user:" + req.params.user);
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.in(socket.room).emit('chat message', msg);
  });
  socket.on('adduser', function(username) {
    socket.username = username;
    console.log("room!!!");
//    socket.room = gid;
    socket.join(gid);
    console.log("con:" + gid + "  user:" + socket.username);
    /*
    if (gid < 1000) {
      console.log("<:" + gid);
      socket.join("room1");
      socket.room = "room1";
    }
    else {
      console.log(">:" + gid);
      socket.join("room2");
      socket.room = "room2";
    }
    */
    //io.emit('chat message', gid);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
